package org.openapitools.codegen.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.oas.models.OpenAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

import org.json.JSONObject;

public class SerializerUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(SerializerUtils.class);

    public static String toYamlString(OpenAPI openAPI) {
        if(openAPI == null) {
            return null;
        }

        SimpleModule module = new SimpleModule("OpenAPIModule");
        module.addSerializer(OpenAPI.class, new OpenAPISerializer());
        try {
            return Yaml.mapper()
                    .registerModule(module)
                    .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
                    .writeValueAsString(openAPI)
                    .replace("\r\n", "\n");
        } catch (JsonProcessingException e) {
            LOGGER.warn("Can not create yaml content", e);
        }
        return null;
    }


    public static String convertYamlToJson(String yaml) {
        ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
        Object obj = null;
        try {
            obj = yamlReader.readValue(yaml, Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectMapper jsonWriter = new ObjectMapper();
        try {
            return jsonWriter.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String asYaml(String jsonString) throws JsonProcessingException, IOException {
        // parse JSON
        JsonNode jsonNodeTree = new ObjectMapper().readTree(jsonString);
        // save it as YAML
        String jsonAsYaml = new YAMLMapper().writeValueAsString(jsonNodeTree);
        return jsonAsYaml;
    }

    //Delete the paths that the parameter Tag doesn't contain
    public static String removeKeyJSON(String jsonInput, String tag) {

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonInput);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<String> keysDelete = new ArrayList<String>();

        JSONObject jRoutePath = null;

        JSONObject jMethod = null;

        JSONArray jTag = null;


        try {
            JSONObject jPaths = (JSONObject) jsonObject.get("paths");

            Iterator<String> keys = jPaths.keys();
            while (keys.hasNext()) {
                String key = keys.next();


                jRoutePath = (JSONObject) jPaths.get(key);
                Iterator<String> keysM = jRoutePath.keys();
                while (keysM.hasNext()) {
                    String keyM = keysM.next();

                    jMethod = (JSONObject) jRoutePath.get(keyM);

                    jTag = (JSONArray) jMethod.get("tags");
                    String tagJSON =jTag.getString(0);
                    tagJSON = tagJSON.toLowerCase(Locale.ROOT);

                    if (!tagJSON.contains(tag.toLowerCase(Locale.ROOT)))
                        keysDelete.add(key);
                }

            }

            for (int i = 0; i < keysDelete.size(); i++) {
                jPaths.remove(keysDelete.get(i));
            }


            return String.valueOf(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }

    //Adds the "Technology" parameter path into specification to Node Aggregator option.
    public static String parseSpecToAggregator(String jsonInput) {

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonInput);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String,JSONObject> keysList = new HashMap<String, JSONObject>();

        JSONObject jRoutePath = null;

        JSONObject jMethod = null;

        JSONArray jParameters = null;


        try {
            JSONObject jPaths = (JSONObject) jsonObject.get("paths");

            Iterator<String> keys = jPaths.keys();
            String techParameter="{\n" +
                    "                  \"description\": \"Choose firebase or mqtt\",\n" +
                    "                  \"in\": \"path\",\n" +
                    "                  \"name\": \"technology\",\n" +
                    "                  \"required\": true,\n" +
                    "                  \"schema\": {\n" +
                    "                     \"type\": \"string\"\n" +
                    "                  }\n" +
                    "               }";
            JSONObject jsonTech= new JSONObject(techParameter);

            while (keys.hasNext()) {
                String key = keys.next();
                System.out.println("key:" + key);
                // jPaths.accumulate("/{tech}"+key,"");


                jRoutePath = (JSONObject) jPaths.get(key);

                keysList.put("/{technology}"+key,jRoutePath);


                Iterator<String> keysM = jRoutePath.keys();
                while (keysM.hasNext()) {
                    String keyM = keysM.next();

                    jMethod = (JSONObject) jRoutePath.get(keyM);
                    if(jMethod.has("parameters")) {
                        jParameters = (JSONArray) jMethod.get("parameters");
                        jParameters.put(jsonTech);

                        System.out.println(jParameters.get(0));
                    }else{
                        jMethod.put("parameters", new JSONArray().put(jsonTech));
                    }

                }

            }
            Iterator it = keysList.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();


                jPaths.accumulate(String.valueOf(pair.getKey()), pair.getValue());
                jPaths.remove(String.valueOf(pair.getKey()).substring(13));

            }



            //System.out.println(keysList.toString());
            return String.valueOf(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }
}
