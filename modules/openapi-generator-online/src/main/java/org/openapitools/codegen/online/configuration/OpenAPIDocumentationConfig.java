/*
 * Copyright 2018 OpenAPI-Generator Contributors (https://openapi-generator.tech)
 * Copyright 2018 SmartBear Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.openapitools.codegen.online.configuration;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import sun.rmi.runtime.Log;

import javax.activation.URLDataSource;
import java.io.InputStream;
import java.util.logging.Logger;


@Configuration
@EnableSwagger2
public class OpenAPIDocumentationConfig {
    @Autowired
    private ResourceLoader resourceLoader;
    URLDataSource source = new URLDataSource(this.getClass().getResource("/static/images/banner-proyecto.png"));





    ApiInfo apiInfo() {

        System.out.println(source.getURL().getHost());
        return new ApiInfoBuilder()
            .title("APIGEND")
            .description("You can find the original version at https://github.com/OpenAPITools/openapi-generator. \n" +
                    " <!DOCTYPE html>\n" +
                    "<html> <h4>Suggestions:</h4> <ul>\n" +
                    "  <li><strong>OpenAPI Url:</strong> <ul>" +
                    "           <li>It is essential to indicate a correct final path of the specification or the generator will return an error 400. For example, in Github, you must obtain the file path in Raw.</li></ul></li>\n" +
                    "  <li><strong>Description of the specification:</strong> <ul>\n" +
                    "      <li>Do not define parameters with the same name and different type.</li>\n" +
                    "      <li>Do not define parameters, operations,... that include underscores as the generator may return an error or generate the API incorrectly.</li>\n" +
                    "    </ul></li>\n" +
                    "</ul>" +
                    "<img src=\"https://github.com/slasom/images/blob/master/banner-4.png?raw=true\" ></html>")
            .license("Apache 2.0")
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
            .termsOfServiceUrl("")
            .version("3.0.0")
            .contact(new Contact("","", ""))
            .build();
    }

    @Bean
    public Docket customImplementation(){

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("org.openapitools.codegen.online.api"))
                    .build()
                .forCodeGeneration(true)
                .directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(java.time.OffsetDateTime.class, java.util.Date.class)
                .directModelSubstitute(JsonNode.class, java.lang.Object.class)
                .ignoredParameterTypes(Resource.class)
                .ignoredParameterTypes(InputStream.class)
                .apiInfo(apiInfo());
    }



}
